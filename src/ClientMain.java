import java.io.IOException;
import java.util.Scanner;


public class ClientMain {
	private static final int PORT = 5001;
	public static void main(String[] args) {
		MyClient client = new MyClient("158.108.42.123", PORT);
		try {
			client.openConnection();
			System.out.println("Connection complete.");
			Scanner input = new Scanner(System.in);
			String s ="";
			while(client.isConnected()){
				s = input.nextLine();
				client.sendToServer(s);
				if( s.equalsIgnoreCase("quit")) client.closeConnection();
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Does not connected.");
		}
	}
}
