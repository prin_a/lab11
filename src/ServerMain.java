import java.io.IOException;


public class ServerMain {
	private static final int PORT = 5001;
	public static void main(String[] args){
		MyServer server = new MyServer(PORT);
		try {
			server.listen();
			System.out.println("Listening on port 5001, IP 158.108.42.123" );
			
		} catch (IOException e) {
			System.out.println("Couldn't start server:");
			System.out.println(e);
		}
	}
}
