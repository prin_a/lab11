import java.io.IOException;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;


public class MyServer extends AbstractServer{

	final static int LOGGEDIN = 2;
	final static int LOGGEDOUT = 0;

	public MyServer(int port) {
		super(port);
		// TODO Auto-generated constructor stub
	}

	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		// TODO Auto-generated method stub
		if ( ! ( msg instanceof String )) {
			sendToClient(client,"Unrecognized Message");

			System.out.println("exit");
			return;
		}

		String message = (String) msg;
		int state = (Integer) client.getInfo("state");
		
		switch(state){
		case LOGGEDOUT:
			if(( message.matches("Login \\w+") )){
				String username = message.substring(6).trim();
				client.setInfo("username", username);
				client.setInfo("state", LOGGEDIN);
				super.sendToAllClients(client.getInfo("username") + " connected");
				System.out.println(client.toString() + " "+ client.getInfo("username") + " connected");
			} else {
				sendToClient(client, "Please login");
				return;
			}
			break;		
		case LOGGEDIN:
			if( message.equalsIgnoreCase("Logout")) {
				client.setInfo("state", LOGGEDOUT);
				sendToClient(client, "Goodbye");
				super.sendToAllClients(client.getInfo("username") + " logged off");
				System.out.println(client.toString() + " " + client.getInfo("username") + " Logged off");
			} else {
				if( message.matches("To: \\w+") ){
					String toUser = message.substring(4).trim();
					client.setInfo("To", toUser);					
				} else if( client.getInfo("To") == null ){
					String username = (String) client.getInfo("username");
					super.sendToAllClients(username + ": " + message);
					System.out.println(client.toString() + " " + username + ": " + message);
				} else {
					boolean success = false;
					for( Thread target :  getClientConnections() ){
						ConnectionToClient to = (ConnectionToClient) target;
						if( to.getInfo("username").equals( client.getInfo("To") )) {
							sendToClient( to, "From: " + client.getInfo("username") + ": " + message );
							success = true;
							System.out.println(client.toString() + " " + client.getInfo("username") + " whisper  to " + to.getInfo("username") + ": " + message);
						}
					}
					if( !success ){
						sendToClient( client, client.getInfo("username") + " does not logged in." );
					}
					client.setInfo("To", null);
				}
				

			}
			break;
		}
	}
	
	protected void clientConnected(ConnectionToClient client) {
		client.setInfo("state", LOGGEDOUT);
	}
	

	protected void sendToClient(ConnectionToClient client, String message){
		try {
			client.sendToClient(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to send Message");
		}
	}

}
